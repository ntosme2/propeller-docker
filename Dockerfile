FROM ubuntu:16.04
MAINTAINER Brian Wightman <brianw1@gmail.com>

# Install Debian dependencies
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -qqy \
    && apt-get install --no-install-recommends -y \
    apt-transport-https \
    ca-certificates \
    git-core \
    wget \
    gcc \
    g++ \
    make \
    autotools-dev \
    libncurses-dev \
    bzip2 \
    libexpat-dev \
    bison \
    flex \
    lib32z1 \
    texinfo \
    lib32stdc++6 \
    libgmp-dev \
    libmpfr-dev \
    libmpc-dev \
    file \
    cmake \
    build-essential

RUN mkdir /tmp/parallax \
    && cd /tmp/parallax \
    && git clone https://github.com/dbetz/propeller-gcc

RUN cd /tmp/parallax/propeller-gcc \
    && git submodule init \
    && git submodule update

RUN cd /tmp/parallax/propeller-gcc/gcc \
    ./contrib/download_prerequisites

RUN cd /tmp/parallax/propeller-gcc \
    && export CFLAGS="-Wno-error -Wno-nonnull" \
    && make GCCDIR=gcc -j`nproc --all` \
    && make GCCDIR=gcc install \
    && cd / \
    && rm -rf /tmp/parallax

RUN apt-get update -qqy \
    && apt-get install --no-install-recommends -y \
    libx11-6 \
    libxext6

RUN cd /tmp \
    && wget https://ci.zemon.name/repository/download/PropWare_Release20/1076:id/PropWare-2.0.1-Generic.deb?guest=1 -O PropWare.deb \
    && dpkg -i PropWare.deb \
    && apt-get install -f

COPY ./entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
CMD ["/bin/bash"]
