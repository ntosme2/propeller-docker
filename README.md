# propeller-docker

Docker to automate building a Parallax Propeller build environment.

Currently includes:
- [propeller-gcc](https://github.com/dbetz/propeller-gcc) (gcc6)
- [PropWare](https://github.com/parallaxinc/PropWare) (2.0.1)
